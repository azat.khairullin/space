package main;

import javax.swing.*;
import java.awt.*;

public class Game extends Canvas implements Runnable {

    private static final long serialVersionUID = 1L;
    public static final int WIDTH = 320;
    public static final int HEIGHT = WIDTH / 12 * 9;
    public static final int SCALE = 2;
    private static final String TITLE = "SPACE";

    private boolean running = false;
    private Thread thread;

    @Override
    public void run() {
        long lastTime = getCurrentTime();
        final double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        int updates = 0;
        int frames = 0;
        long timer = System.currentTimeMillis();
        while (running) {
            long now = getCurrentTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if (delta >= 1) {
                tick();
                updates++;
                delta--;
            }
            render();
            frames++;

            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println("Ticks= " + updates + " Fps= " + frames);
                updates = 0;
                frames = 0;
            }
        }

        stop();
    }

    private void render() {

    }


    private void tick() {

    }

    private long getCurrentTime() {
        return System.nanoTime();
    }

    private synchronized void start() {
        if (running) {
            return;
        }

        running = true;
        thread = new Thread(this);
        thread.start();
    }

    private synchronized void stop() {
        if (!running) {
            return;
        }

        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.exit(1);
    }

    public static void main(String[] args) {
        Game game = new Game();

        game.injectDimension(game);
        game.builder(game);

        game.start();

    }

    private void injectDimension(Game game) {
        game.setPreferredSize(getDimension());
        game.setMaximumSize(getDimension());
        game.setMinimumSize(getDimension());
    }

    private Dimension getDimension() {
        return new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
    }

    private JFrame builder(Game game) {
        JFrame jf = new JFrame(TITLE);
        jf.add(game);
        jf.pack();
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setResizable(false);
        jf.setLocationRelativeTo(null);
        jf.setVisible(true);

        return jf;
    }
}
